package com.tsystems.sbb.dal;

import com.tsystems.sbb.model.Train;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by rsergeev on 05.08.2015.
 */
public interface TrainRepository extends CrudRepository<Train, Long> {
}
