package com.tsystems.sbb.services;

import com.tsystems.sbb.dal.TrainRepository;
import com.tsystems.sbb.model.Train;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Created by rsergeev on 05.08.2015.
 */
@Service
public class TrainServiceImpl implements TrainService {

//    @Autowired
//    private TrainRepository trainRepository;

    public List<Train> getAllTrains() {
        return Collections.emptyList();
    }
}
