package com.tsystems.sbb.services;

import com.tsystems.sbb.model.Train;

import java.util.List;

/**
 * Created by rsergeev on 05.08.2015.
 */
public interface TrainService {
    public List<Train> getAllTrains();
}
