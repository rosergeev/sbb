package com.tsystems.sbb.controllers;

import com.tsystems.sbb.services.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by rsergeev on 05.08.2015.
 */
@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    TrainService trainService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(){
        return "index";
    }
}
